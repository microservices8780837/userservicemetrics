# userservicemetrics

# Sources

1. https://habr.com/ru/articles/548700/
2. https://gitlab.com/microservices8780837/teachersmaterial/-/blob/main/msa/prometheus/actions.txt
3. https://observability.thomasriley.co.uk/prometheus/configuring-prometheus/using-service-monitors/
4. https://milindasenaka96.medium.com/setup-prometheus-and-grafana-to-monitor-the-k8s-cluster-e1d35343d7a9#:~:text=Open%20your%20web%20browser%20and,components%20which%20are%20Prometheus%20Targets
5. https://earthly.dev/blog/grafana-and-prometheus-k8s/#:~:text=Open%20a%20web%20browser%2C%20and,you’re%20using%20a%20cloud%20server
6. https://docs.micrometer.io/micrometer/reference/concepts.html - Prometheus for Java
7. https://yandex.ru/video/preview/55292589277281625 - @Timed аннотация
8. https://www.youtube.com/watch?v=WQmpeOvCCUY - Prometheus и PromQL — основы сбора метрик
9. https://www.youtube.com/watch?v=91c4YezUhEE - PromQL
10. https://bootcamptoprod.com/spring-boot-2-add-tags-in-server-requests-metrics/
11. https://stackoverflow.com/questions/51552889/how-to-define-additional-or-custom-tags-for-default-spring-boot-2-metrics


# Домашнее задание
Prometheus. Grafana

### Цель:
В этом ДЗ вы научитесь инструментировать сервис.


### Описание/Пошаговая инструкция выполнения домашнего задания:
Инструментировать сервис из прошлого задания метриками в формате Prometheus с помощью библиотеки для вашего фреймворка и ЯП.\
Сделать дашборд в Графане, в котором были бы метрики с разбивкой по API методам:

1. Latency (response time) с квантилями по 0.5, 0.95, 0.99, max
2. RPS
3. Error Rate - количество 500ых ответов\
Добавить в дашборд графики с метрикам в целом по сервису, взятые с nginx-ingress-controller:
4. Latency (response time) с квантилями по 0.5, 0.95, 0.99, max
5. RPS
6. Error Rate - количество 500ых ответов\
Настроить алертинг в графане на Error Rate и Latency.

На выходе должно быть:
0. скриншоты дашборды с графиками в момент стресс-тестирования сервиса. Например, после 5-10 минут нагрузки.
1. json-дашборды.

Задание со звездочкой (+5 баллов)\
Используя существующие системные метрики из кубернетеса, добавить на дашборд графики с метриками:
1. Потребление подами приложения памяти
2. Потребление подами приложения CPU\
Инструментировать базу данных с помощью экспортера для prometheus для этой БД.\
Добавить в общий дашборд графики с метриками работы БД.\
Альтернативное задание на 1 балл (если не хочется самому ставить prometheus в minikube)
- https://www.katacoda.com/schetinnikov/scenarios/prometheus-client

---

# Описание

Golden signals:
1. Latency - как долго обрабатывает запрос
2. Traffic - сколько в секунду приходит
3. Errors -  сколько ошибок
4. Saturation - насколько загружена система, процессор

### pom.xml

Spring boot предоставляет механизм, который позволяет получать различные метаданные приложения. Для этого необходимо добавить зависимость.

```html
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```

Micrometer по-умолчанию включен в зависимости spring boot, нам лишь необходимо добавить адаптер в необходимый нам формат, так как в руководстве использован Prometheus необходимо добавить следующую зависимость.

```html
<dependency>
    <groupId>io.micrometer</groupId>
    <artifactId>micrometer-registry-prometheus</artifactId>
    <scope>runtime</scope>
</dependency>
```

---

### application.yml

Далее необходимо отредактировать файл `application.yml` для отображения эндпоинта Prometheus в actuator.\
Здесь включены `health,prometheus` эндпоинты в `actuator`. Включен экспорт метрик в Prometheus, а также `percentiles-histogram`, которая позволяет верхнеуровнего оценить sla эндпоинтов приложения.\
Теперь, если запустить приложение и зайти по адресу `http://localhost:8080/actuator/prometheus` должен отобразиться вывод примерно следующего содержания.\
Тут видна особенность, что метрики jvm доступны по-умолчанию.

```yaml
management:
  endpoint:
    health:
      show-details: always
    prometheus:
      enabled: true
    metrics:
      enabled: true
  metrics:
    distribution:
      percentiles-histogram:
        "[http.server.requests]": true
      slo:
        http.server.requests: 10ms, 50ms, 100ms, 200ms, 300ms, 400ms, 500ms, 600ms, 800ms
  endpoints:
    web:
      exposure:
        include: health,info,prometheus
```

---

# 1. Запуск служб и сервисов

0. Устанавливаем nginx-ingress из nginx-ingress.yaml
```
kubectl create namespace nginx-ingress && helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx/ && helm repo update && helm install nginx ingress-nginx/ingress-nginx --namespace nginx-ingress -f nginx-ingress.yaml
```

1. Подключаем репо prometheus-community

```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm show values prometheus-community/prometheus
```

---------- Вариант 2 - устанавливаем весь стек - прометей + оператор + графана

2. Устанавливаем prometheus используя /userservicemetrics/prometheus/prometheus.yaml

```
helm install stack prometheus-community/kube-prometheus-stack -f ./manifests/prometheus/prometheus.yaml
```

Результат:
```
$ kubectl get all
NAME                                                         READY   STATUS             RESTARTS      AGE
pod/alertmanager-stack-kube-prometheus-stac-alertmanager-0   2/2     Running            0             6m47s
pod/prometheus-stack-kube-prometheus-stac-prometheus-0       2/2     Running            0             6m47s
pod/stack-grafana-97c746786-fppsk                            3/3     Running            0             7m21s
pod/stack-kube-prometheus-stac-operator-6d95f9d4c8-rvzht     1/1     Running            0             7m21s
pod/stack-kube-state-metrics-ff77b5ffc-9z9lt                 0/1     ImagePullBackOff   0             7m21s
pod/stack-prometheus-node-exporter-f9trl                     0/1     CrashLoopBackOff   6 (76s ago)   7m21s

NAME                                              TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
service/alertmanager-operated                     ClusterIP   None             <none>        9093/TCP,9094/TCP,9094/UDP   6m47s
service/kubernetes                                ClusterIP   10.96.0.1        <none>        443/TCP                      124d
service/prometheus-operated                       ClusterIP   None             <none>        9090/TCP                     6m47s
service/stack-grafana                             ClusterIP   10.102.73.133    <none>        80/TCP                       7m21s
service/stack-kube-prometheus-stac-alertmanager   ClusterIP   10.106.75.200    <none>        9093/TCP,8080/TCP            7m21s
service/stack-kube-prometheus-stac-operator       ClusterIP   10.99.136.84     <none>        443/TCP                      7m21s
service/stack-kube-prometheus-stac-prometheus     ClusterIP   10.102.22.229    <none>        9090/TCP,8080/TCP            7m21s
service/stack-kube-state-metrics                  ClusterIP   10.97.84.94      <none>        8080/TCP                     7m21s
service/stack-prometheus-node-exporter            ClusterIP   10.104.110.220   <none>        9100/TCP                     7m21s

NAME                                            DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
daemonset.apps/stack-prometheus-node-exporter   1         1         0       1            0           kubernetes.io/os=linux   7m21s

NAME                                                  READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/stack-grafana                         1/1     1            1           7m21s
deployment.apps/stack-kube-prometheus-stac-operator   1/1     1            1           7m21s
deployment.apps/stack-kube-state-metrics              0/1     1            0           7m21s

NAME                                                             DESIRED   CURRENT   READY   AGE
replicaset.apps/stack-grafana-97c746786                          1         1         1       7m21s
replicaset.apps/stack-kube-prometheus-stac-operator-6d95f9d4c8   1         1         1       7m21s
replicaset.apps/stack-kube-state-metrics-ff77b5ffc               1         1         0       7m21s

NAME                                                                    READY   AGE
statefulset.apps/alertmanager-stack-kube-prometheus-stac-alertmanager   1/1     6m47s
statefulset.apps/prometheus-stack-kube-prometheus-stac-prometheus       1/1     6m47s
```

3. Устанавливаем userservice с DB
```
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update

helm install userservice-app-postgresql bitnami/postgresql --set image.tag=14.10.0-debian-11-r17,auth.username=sa,auth.password=sa,auth.database=userservice-app && kubectl apply -f ./manifests/helm/userservice-app/postgres-service.yaml

helm install userservice-app ./manifests/helm/userservice-app/

###
helm uninstall userservice-app-postgresql
kubectl delete svc userservice-app-postgresql
helm uninstall userservice-app
kubectl delete pvc data-userservice-app-postgresql-0
curl http://arch.homework/user/1
```
a
4. Устанавливаем ServiceMonitor. spec: endpoints: - port: app-http должен совпадать с ports: - name: app-http сервиса приложения
```
kubectl apply -f ./manifests/prometheus/service-monitor.yaml
kubectl get servicemonitors
```

5. Предоставить внешний доступ к prometheus:
```
kubectl port-forward service/prometheus-operated 9090
OR
kubectl apply -f ./manifests/prometheus/prometheus-service.yaml
```

6. Предоставить внешний доступ к grafana:
```
kubectl port-forward service/stack-grafana 9000:80
OR
kubectl apply -f ./manifests/prometheus/grafana-service.yaml
```

# Debug:
```
kubectl get pods

kubectl exec -it userservice-app-v1-7964bb6fd-rs85r -- /bin/bash

kubectl logs userservice-app-v1-7964bb6fd-rs85r

docker update --restart=no k8s_postgresql_authservice-app-postgresql-0_auth_b08a5368-8795-4cab-86e0-78efe3e17918_13

docker rm -f 59b2c5cff4b5

kubectl -n ingress-nginx get events --sort-by='{.lastTimestamp}'

helm repo list

helm list

```

# Test:

GET user/1
```
http://arch.homework/user/1
```

Actuator:
```
http://arch.homework/actuator
```

Prometheus:
```
http://arch.homework/actuator/prometheus
```

Prometheus UI:
```
http://localhost:9090
OR
http://arch.homework:9090
```

---

# PromQL

Фильтрация по поду:
```
process_cpu_usage{pod="userservice-app-v1-7964bb6fd-d24tc"}
```

Общее количество запросов, полученных сервером в секунду (количество):
```
http_server_requests_seconds_count{uri=~"/user.*"}
```

Общее время ответа на запросы (время):
```
http_server_requests_seconds_sum{uri=~"/user.*"}
```

The rate() function in Prometheus looks at the history of time series over a time period, and calculates how fast it's increasing per second. This can handle multiple Prometheus servers taking samples, and if a scrape fails you'll lose resolution but not data as on the next successful scrape the increments haven't been lost or averaged away.

In addition to rate(), there's increase() and irate() which are the other two functions which operate on counters. While rate() returns per second values, increase() is a convenience function which returns the total across the time period. So for example increase(my_counter_total[1h]) would return the increase in the counter per hour. irate() was discussed previously, it only looks at the last two samples and is useful for high precision graphs over short time frames. resets() tells you how often the counter resets, which can be useful for debugging.

Суммировать результаты rate за 5м
```
sum by (uri) (rate(http_server_requests_seconds_count{uri=~"/user.*"}[5m]))
```

Посчитать среднее время выполнения 1 запроса, время/количество в минуту:
sum by (uri) (rate(http_server_requests_seconds_sum{uri=~"/user.*"}[1m]))/sum by (uri) (rate(http_server_requests_seconds_count{uri=~"/user.*"}[1m]))


Uptime:
```
process_uptime_seconds{instance="10.1.0.111:8000", service="userservice-app-v1"}
```

Start time:
```
process_start_time_seconds{instance="10.1.0.111:8000", service="userservice-app-v1"}*1000
```

Heap used:
```
sum(jvm_memory_used_bytes{instance="10.1.0.111:8000", service="userservice-app-v1", area="heap"})*100/sum(jvm_memory_max_bytes{instance="10.1.0.111:8000", service="userservice-app-v1", area="heap"})
```


```
sum(rate(http_server_requests_seconds_countinstance="10.1.0.111:8000", service="userservice-app-v1"}[1m]))
```

Duration(Посчитать среднее время выполнения 1 запроса, время/количество в минуту):
```
sum(rate(http_server_requests_seconds_sum{instance="10.1.0.111:8000", service="userservice-app-v1", status!~"5.."}[1m]))/sum(rate(http_server_requests_seconds_count{instance="10.1.0.111:8000", service="userservice-app-v1", status!~"5.."}[1m]))
```

rate() - агрегированая функция, скорость изменений в секунду. 
rate(req_total[15m] - показать как изменялась скорость в течении 15 минут, и раздели на 15, чтобы получить скорость изменений в секунду
sum(rate(req_total[15s])) - суммирует все строки
sum by(server) (rate(req_total[15m])) - посчитает по разным серверам, выведет общее количество для 2 элементов
sum(rate(req_total{code=~"4..|5.."}[15s])) - количество ошибок
sum(rate(req_total{code=~"4..|5.."}[15s]))/sum(rate(req_total{}))*100 - количество ошибок/все ошибки * 100 - процент ошибок
sum(rate(req_total{code=~"4..|5.."}[15s])) by (handler) / sum(rate(req_total{})) by (handler) * 100 - количество ошибок/все ошибки * 100 - процент ошибок
100 - avg(rate(node_cpu_seconds_total{mode="idle"}[15s])) * 100 - сколько процессор работает в процентах
avg(rate(node_cpu_seconds_total{mode="idle"}[15s])) * 100 - сколько процентов процессор отдыхает
100 - node_memory_MemFree_bytes / node_memory_MemTotal_bytes * 100 - сколько процент памяти занятой
node_memory_MemFree_bytes / node_memory_MemTotal_bytes * 100 - сколько процент памяти свободной

![img.png](img.png)


Гистограмма (Latency) - измеряет статистику по какой-то величине, например какой процент запросов был быстрее чем 100 ms
Суммирует все значения sum которые добавлены потом count на 1.
Если запрос выполнялся 80ms, то 80 + sum, и count + 1.
Среднее значение = sum / count.
Быстрее чем 75 ms, каждый запрос добавляет в bucket, le="0,08" +=1
```
sum(rate(products_app_requests_duration_sum[15s]))/sum(rate(products_app_requests_duration_count[15s])) * 1000 - среднее время выполнения запроса в ms
```
sum by (handler) (rate(products_app_requests_duration_sum{code=~"2.."}[15s]))/sum by (handler) (rate(products_app_requests_duration_count[15s])) * 1000 - среднее время выполнения запроса в ms

функция increase(), которая вычислит разницу между значениями счетчика в начале и в конце указанного временного интервала. Он также корректно обрабатывает сброс счетчика в течение этого периода времени (если таковой имеется).

# Grafana

username:
```
admin
```

password:
```
prom-operator
```

---

Получить 500
```
sum by (status) (increase(nginx_ingress_controller_request_duration_seconds_count{status=~"5.+"}[1m]))

```

# Для фильтрации по тэгам
1. Можно добавить компонент
```java
@Component
public class MeterConfig {

	@Value("${spring.application.name}")
	private String applicationName;

	public MeterConfig(MeterRegistry registry) {
		registry.config().meterFilter(MeterFilter.replaceTagValues("application", s -> applicationName));
	}
}
```

Добавить/обновить тэг WebMvcTagsProvider:
```java
public class DefaultWebMvcTagsProvider implements WebMvcTagsProvider {

    @Override
    public Iterable<Tag> getTags(HttpServletRequest request, HttpServletResponse response, Object handler,
            Throwable exception) {
        Tags tags = Tags.of(WebMvcTags.method(request), WebMvcTags.uri(request, response), WebMvcTags.exception(exception),
                WebMvcTags.status(response), WebMvcTags.outcome(response));
        if (handler instanceof HandlerMethod) {
  			  HandlerMethod handlerMethod = (HandlerMethod) handler;
	  		  String package = handlerMethod.getBeanType().getCanonicalName();
          tags = tags.and(Tag.of("package", package));
			}
		}

    @Override
    public Iterable<Tag> getLongRequestTags(HttpServletRequest request, Object handler) {
        return Tags.of(WebMvcTags.method(request), WebMvcTags.uri(request, null));
    }
}
```

application.properties:
```
management.endpoints.jmx.exposure.include=*
management.endpoints.web.exposure.include=health,info,prometheus
management.endpoint.metrics.enabled=true
server.tomcat.mbeanregistry.enabled=true
management.endpoint.prometheus.enabled=true
management.metrics.tags.application=${spring.application.name}
management.metrics.tags.module=Unknown
management.metrics.tags.profile=${spring.profiles.active}
management.endpoint.health.show-details=always
management.metrics.distribution.slo.http.server.requests: 10ms, 50ms, 100ms, 200ms, 300ms, 400ms, 500ms, 600ms, 800ms
```

Кастомный счетчик:
```
@RestController
public class MyController {

  @Autowired
  MeterRegistry registry;

  @RequestMapping(value = "/message/{message}", method = RequestMethod.GET)
  public String getMessage(@PathVariable("message") String message) {

    // counter to count different types of messages received
    registry.counter("custom.metrics.message", "value", message).increment();

    return message;
  }
```