package com.otus.userservice.service;

import static java.lang.String.format;
import static java.util.Optional.ofNullable;

import com.otus.userservice.dto.UserCreationRequestDto;
import com.otus.userservice.dto.UserDto;
import com.otus.userservice.dto.UserUpdatingRequestDto;
import com.otus.userservice.entity.User;
import com.otus.userservice.mapper.UserMapper;
import com.otus.userservice.repository.UserRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

  @Autowired
  private UserMapper userMapper;

  @Autowired
  private UserRepository userRepository;

  public UserDto saveUser(UserCreationRequestDto dto) {
    User user = userMapper.userCreationDtoToUser(dto);
    return userMapper.userToUserDto(userRepository.save(user));
  }

  public UserDto getUserById(Long userId) {
    return userMapper.userToUserDto(userRepository
        .findById(userId)
        .orElseThrow(() -> new EntityNotFoundException(format("User with id %d not found", userId))));
  }

  public void deleteUser(Long userId) {
    userRepository.deleteById(userId);
  }

  public UserDto updateUser(Long userId, UserUpdatingRequestDto dto) {
    User user = userRepository
        .findById(userId)
        .orElseThrow(() -> new EntityNotFoundException(format("User with id %d not found", userId)));
    ofNullable(dto.getEmail()).ifPresent(user::setEmail);
    ofNullable(dto.getFirstName()).ifPresent(user::setFirstName);
    ofNullable(dto.getLastName()).ifPresent(user::setLastName);
    ofNullable(dto.getPhone()).ifPresent(user::setPhone);
    User updatedUser = userRepository.save(user);
    return userMapper.userToUserDto(updatedUser);
  }
}
